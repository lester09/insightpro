﻿using insightpro_api.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using insightpro_api.Helpers;

namespace insightpro_api.Controllers
{

    public class ScoringFactorList
    {
        public int scoring_factor_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int? benchmark_value_min { get; set; }
        public int? benchmark_value_max { get; set; }
        public int? tolerance_value { get; set; }
        public bool? calculated { get; set; }
        public double? weight { get; set; }
        
    }

    public class AssetBenchmarkList
    {
        public int asset_benchmark_id { get; set; }
        public int scoring_factor_id { get; set; }
        public string name { get; set; }
        public string asset_id { get; set; }
        public bool? calculated { get; set; }
        public double? benchmark_value_min { get; set; }
        public double? benchmark_value_max { get; set; }
        public DateTime last_updated_dt { get; set; }
        public double? weight { get; set; }
        public int profile_id { get; set; }
    }

    public class DriverToleranceList
    {
        public int driver_tolerance_id { get; set; }
        public int scoring_factor_id { get; set; }
        public string driver_id { get; set; }
        public string name { get; set; }
        public bool? calculated { get; set; }
        public double? tolerance_value_min { get; set; }
        public double? tolerance_value_max { get; set; }
        public DateTime last_updated_dt { get; set; }
        public double? weight { get; set; }
        public int profile_id { get; set; }
    }

    public class ProfileAll
    {
        public int profile_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int user_id { get; set; }
        public string type { get; set; }
        public bool isAssign { get; set; }
        public List<AssetBenchmarkList> AssetBenchmarkLists { get; set; }
        public List<ScoringFactorList> ScoringFactorLists { get; set; }
        public List<DriverToleranceList> DriverToleranceLists { get; set; }

        public ProfileAll()
        {
            this.ScoringFactorLists = new List<ScoringFactorList>();
            this.AssetBenchmarkLists = new List<AssetBenchmarkList>();
            this.DriverToleranceLists = new List<DriverToleranceList>();
        }

    }

    public class ProfileList
    {
        public int profile_id { get; set; }
        public string name { get; set; }
        public bool isAssign { get; set; }
    }

    public class ProfileUpdate_param
    {
        public int profile_id { get; set; }
        public string asset_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool td_calculated { get; set; }
        public bool id_calculated { get; set; }
        public bool pd_calculated { get; set; }
        public bool dt_calculated { get; set; }
        public bool vc_calculated { get; set; }
        public bool tc_calculated { get; set; }
        public bool tzc_calculated { get; set; }
        public double td_bm { get; set; }
        public double id_bm { get; set; }
        public double pd_bm { get; set; }
        public double dt_bm { get; set; }
        public double vc_bm { get; set; }
        public double tc_bm { get; set; }
        public double tzc_bm { get; set; }
        public double td_weight { get; set; }
        public double id_weight { get; set; }
        public double pd_weight { get; set; }
        public double dt_weight { get; set; }
        public double vc_weight { get; set; }
        public double tc_weight { get; set; }
        public double tzc_weight { get; set; }
    }

    public class ProfileAssign
    {
        public int? profile_id { get; set; }
        public string[] asset_ids { get; set; }
    }

    public class ProfileDriverAssign
    {
        public int? profile_id { get; set; }
        public int[] driver_ids { get; set; }
    }



    [RoutePrefix("Profile")]
    public class ProfileController : ApiController
    {


        #region asset

        [Authorize]
        [Route("Asset/List")]
        [HttpGet]
        public IHttpActionResult ProfileList()
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var assetbenchmarklist = isdb.AssetBenchmark.ToList();

                var scoringfactorlist = isdb.ScoringFactor.ToList();

                var profile = (from i in isdb.Profile
                               where i.user_id == AccountID && i.type == "asset"
                               select new ProfileAll()
                               {
                                   profile_id = i.profile_id,
                                   name = i.name,
                                   description = i.description,
                                   user_id = i.user_id ?? 0,
                                   type = i.type
                               }).OrderBy(x => x.name).ToList();


                profile.ForEach(b => b.AssetBenchmarkLists.AddRange(
                    assetbenchmarklist.Where(x => x.profile_id == b.profile_id).Select(c => new AssetBenchmarkList()
                    {
                        asset_benchmark_id = c.asset_benchmark_id,
                        scoring_factor_id = c.scoring_factor_id,
                        name = c.ScoringFactor.name,
                        asset_id = c.asset_id,
                        calculated = c.calculated,
                        benchmark_value_min = c.benchmark_value_min ?? 0,
                        benchmark_value_max = c.benchmark_value_max ?? 0,
                        last_updated_dt = c.last_updated_dt,
                        weight = c.weight,
                        profile_id = c.profile_id ?? 0
                    }).ToList().DistinctBy(x => x.name)
                    ));


                return Ok(profile);

            }
        }

        [Authorize]
        [Route("Asset/Info")]
        [HttpGet]
        public IHttpActionResult ProfileInfo(int profile_id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.Profile.Where(x => x.profile_id == profile_id).ToList();

                if (checkifExist.Count() > 0)
                {
                    var profile = checkifExist.Select(i => new ProfileAll()
                    {
                        profile_id = i.profile_id,
                        name = i.name,
                        description = i.description,
                        user_id = i.user_id ?? 0,
                    }).FirstOrDefault();

                    profile.AssetBenchmarkLists.AddRange(
                        isdb.AssetBenchmark.Where(x => x.profile_id == profile.profile_id).Select(c => new AssetBenchmarkList()
                        {
                            asset_benchmark_id = c.asset_benchmark_id,
                            scoring_factor_id = c.scoring_factor_id,
                            name = c.ScoringFactor.name,
                            asset_id = c.asset_id,
                            calculated = c.calculated,
                            benchmark_value_max = c.benchmark_value_max ?? 0,
                            benchmark_value_min = c.benchmark_value_min ?? 0,
                            last_updated_dt = c.last_updated_dt,
                            weight = c.weight,
                            profile_id = c.profile_id ?? 0
                        }).ToList().DistinctBy(x=> x.name)
                    );

                    return Ok(profile);
                }
                else
                {
                    return BadRequest("Profile not found.");
                }
            }
        }

        [Authorize]
        [Route("Asset/Add")]
        [HttpPost]
        public IHttpActionResult AddProfile(ProfileAll _prof)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var checkifProfilenameExist = isdb.Profile.Where(x => x.name == _prof.name && x.type == "asset" && x.user_id == AccountID).ToList();

                if (checkifProfilenameExist.Count() > 0)
                {
                    return BadRequest("Profile name is already exists.");
                }

                Profile _profile = new Profile()
                {
                    name = _prof.name,
                    description = _prof.description,
                    user_id = AccountID,
                    type = "asset"
                };
                isdb.Profile.Add(_profile);


                foreach (var a in _prof.AssetBenchmarkLists.ToList())
                {
                    AssetBenchmark abm = new AssetBenchmark()
                    {
                        scoring_factor_id = a.scoring_factor_id,
                        calculated = a.calculated ?? false,
                        benchmark_value_min = a.benchmark_value_min,
                        benchmark_value_max = a.benchmark_value_max,
                        last_updated_dt = DateTime.Now,
                        weight = a.weight ?? 0,
                        profile_id = _profile.profile_id
                    };

                    isdb.AssetBenchmark.Add(abm);

                }

                isdb.SaveChanges();

                _prof.profile_id = _profile.profile_id;

                return Ok(_prof);
            }
        }

        [Authorize]
        [Route("Asset/Assign")]
        [HttpPost]
        public IHttpActionResult AssignProfile(ProfileAssign _profassign)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var profile = isdb.ProfileAssets.ToList();
                profile.RemoveAll(x => x.profile_id == _profassign.profile_id);


                var _profassets = isdb.ProfileAssets.Where(x => _profassign.asset_ids.Contains(x.asset_id)).ToList();

                //_profassets.ForEach(x => isdb.ProfileAssets.Remove(x));
                if (_profassets.Count() == 0)
                {
                    var assets = _profassign.asset_ids;

                    if (_profassign.profile_id != null)
                    {
                        foreach (var a in assets)
                        {
                            ProfileAssets _prof = new ProfileAssets()
                            {
                                profile_id = _profassign.profile_id,
                                asset_id = a
                            };


                            isdb.ProfileAssets.Add(_prof);
                        }
                    }

                    isdb.SaveChanges();

                    return Ok("Assign successfully.");
                }
                else
                {
                    return BadRequest("Already assigned");
                }

               
              

               

            }
        }


        [Authorize]
        [Route("Asset/DeAssign")]
        [HttpPost]
        public IHttpActionResult DeAssignProfile(ProfileAssign _profassign)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var _profassets = isdb.ProfileAssets.Where(x => _profassign.asset_ids.Contains(x.asset_id) && x.profile_id == _profassign.profile_id).ToList();

                _profassets.ForEach(x => isdb.ProfileAssets.Remove(x));

                isdb.SaveChanges();

                return Ok("Remove Assignment successfully.");
            }
        }

        //[Authorize]
        //[Route("Asset/Assign/List")]
        //[HttpGet]
        //public IHttpActionResult AssignProfileList(int? profile_id)
        //{
        //    var identity = (ClaimsIdentity)User.Identity;
        //    IEnumerable<Claim> Claims = identity.Claims;
        //    int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
        //    string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
        //    string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

        //    using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
        //    {
        //        using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
        //        {

        //            var result = (from i in tfdb.sp_PGPS_get_summary_info_sysuser(AccountID).ToList()
        //                          select new Asset()
        //                          {
        //                              asset_id = i.AssetID,
        //                              asset_name = i.Name,
        //                              customer_id = i.CustomerID,
        //                              customer_name = i.CustomerName,
        //                          }).OrderBy(x => x.asset_name).ToList();

        //            var profasset = isdb.ProfileAssets.Where(x => x.profile_id == profile_id).ToList();

        //            if (profasset.Count() > 0)
        //            {
        //                foreach (var r in result.ToList())
        //                {
        //                    r.isAssign = profasset.ToList().Where(x => x.asset_id == r.asset_id).ToList().Count() > 0 ? true : false;
        //                }

        //            }
        //            else
        //            {
        //                result.ForEach(x => x.isAssign = false);
        //            }

        //            return Ok(result);
        //        }



        //    }

        //}

        [Authorize]
        [HttpGet]
        [Route("Asset/Assign/List")]
        public IHttpActionResult AssignProfileList(string asset_id)
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var assetbenchmarklist = isdb.AssetBenchmark.ToList();

                //var scoringfactorlist = isdb.ScoringFactor.ToList();

                var profile = (from i in isdb.Profile
                               where i.user_id == AccountID && i.type == "asset"
                               select new ProfileAll()
                               {
                                   profile_id = i.profile_id,
                                   name = i.name,
                                   description = i.description,
                                   user_id = i.user_id ?? 0,
                                   type = i.type,
                                   //isAssign = isdb.ProfileAssets.Where(x=> x.profile_id == i.profile_id && x.asset_id == asset_id).ToList().Count() > 0 ? true : false
                               }).OrderBy(x => x.name).ToList();


                profile.ForEach(x => x.isAssign = isdb.ProfileAssets.Where(c => c.profile_id == x.profile_id && c.asset_id == asset_id).Count() > 0 ? true : false);

                profile.ForEach(b=> b.AssetBenchmarkLists.AddRange(

                    assetbenchmarklist.Where(x => x.profile_id == b.profile_id).Select(c => new AssetBenchmarkList()
                    {
                        asset_benchmark_id = c.asset_benchmark_id,
                        scoring_factor_id = c.scoring_factor_id,
                        name = c.ScoringFactor.name,
                        asset_id = c.asset_id,
                        calculated = c.calculated,
                        benchmark_value_min = c.benchmark_value_min ?? 0,
                        benchmark_value_max = c.benchmark_value_max ?? 0,
                        last_updated_dt = c.last_updated_dt,
                        weight = c.weight,
                        profile_id = c.profile_id ?? 0
                    }).ToList().DistinctBy(x => x.name)
                    ));


                return Ok(profile);

            }

        }

        [Authorize]
        [HttpPost]
        [Route("Asset/Update")]
        public IHttpActionResult UpdateProfile(ProfileAll _prof)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {


                var checkifExist = isdb.Profile.Where(x => x.profile_id == _prof.profile_id).ToList();


                if (checkifExist.Count() > 0)
                {

                    var checkifProfilenameExist = isdb.Profile.Where(x => x.name == _prof.name && x.type == "asset" && x.user_id == AccountID && x.profile_id != _prof.profile_id).ToList();

                    if (checkifProfilenameExist.Count() > 0)
                    {
                        return BadRequest("Profile name is already exists");
                    }

                    var profile = checkifExist.FirstOrDefault();
                    profile.name = _prof.name;
                    profile.description = _prof.description;

                    isdb.SaveChanges();

                    var abm = isdb.AssetBenchmark.Where(x => x.profile_id == _prof.profile_id).ToList();

                    if (_prof.AssetBenchmarkLists.Count() > 0)
                    {
                        var td = _prof.AssetBenchmarkLists.ToList().Where(x => x.name == "Travel Duration").ToList();
                        if (td.Count() > 0)
                        {
                            var tdbm = abm.Where(x => x.scoring_factor_id == td.FirstOrDefault().scoring_factor_id).ToList();

                            if (tdbm.Count() > 0)
                            {
                                tdbm.FirstOrDefault().weight = td.FirstOrDefault().weight ?? 0;
                                tdbm.FirstOrDefault().benchmark_value_min = td.FirstOrDefault().benchmark_value_min ?? 0;
                                tdbm.FirstOrDefault().benchmark_value_max = td.FirstOrDefault().benchmark_value_max ?? 0;
                                tdbm.FirstOrDefault().last_updated_dt = DateTime.Now;
                                tdbm.FirstOrDefault().calculated = td.FirstOrDefault().calculated ?? false;

                            }
                        }
                      

                        var tpd = _prof.AssetBenchmarkLists.ToList().Where(x => x.name == "Trip Duration").ToList();
                        if (tpd.Count() > 0)
                        {
                            var tpdbm = abm.Where(x => x.scoring_factor_id == tpd.FirstOrDefault().scoring_factor_id).ToList();
                            if (tpdbm.Count() > 0)
                            {
                                tpdbm.FirstOrDefault().weight = tpd.FirstOrDefault().weight ?? 0;
                                tpdbm.FirstOrDefault().benchmark_value_min = tpd.FirstOrDefault().benchmark_value_min ?? 0;
                                tpdbm.FirstOrDefault().benchmark_value_max = tpd.FirstOrDefault().benchmark_value_max ?? 0;
                                tpdbm.FirstOrDefault().last_updated_dt = DateTime.Now;
                                tpdbm.FirstOrDefault().calculated = tpd.FirstOrDefault().calculated ?? false;
                            }
                        }
                        

                        var dt = _prof.AssetBenchmarkLists.ToList().Where(x => x.name == "Distance Travelled").ToList();
                        if (dt.Count() > 0)
                        {
                            var dtbm = abm.Where(x => x.scoring_factor_id == dt.FirstOrDefault().scoring_factor_id).ToList();
                            if (dtbm.Count() > 0)
                            {
                                dtbm.FirstOrDefault().weight = dt.FirstOrDefault().weight ?? 0;
                                dtbm.FirstOrDefault().benchmark_value_min = dt.FirstOrDefault().benchmark_value_min ?? 0;
                                dtbm.FirstOrDefault().benchmark_value_max = dt.FirstOrDefault().benchmark_value_max ?? 0;
                                dtbm.FirstOrDefault().last_updated_dt = DateTime.Now;
                                dtbm.FirstOrDefault().calculated = dt.FirstOrDefault().calculated ?? false;
                            }
                        }


                        var tc = _prof.AssetBenchmarkLists.ToList().Where(x => x.name == "Trip Count").ToList();
                        if (tc.Count() > 0)
                        {
                            var tcbm = abm.Where(x => x.scoring_factor_id == tc.FirstOrDefault().scoring_factor_id).ToList();
                            if (tcbm.Count() > 0)
                            {
                                tcbm.FirstOrDefault().weight = tc.FirstOrDefault().weight ?? 0;
                                tcbm.FirstOrDefault().benchmark_value_min = tc.FirstOrDefault().benchmark_value_min ?? 0;
                                tcbm.FirstOrDefault().benchmark_value_max = tc.FirstOrDefault().benchmark_value_max ?? 0;
                                tcbm.FirstOrDefault().last_updated_dt = DateTime.Now;
                                tcbm.FirstOrDefault().calculated = tc.FirstOrDefault().calculated ?? false;
                            }
                        }
                     

                        var tzc = _prof.AssetBenchmarkLists.ToList().Where(x => x.name == "Trip Zone Count").ToList();
                        if (tzc.Count() > 0)
                        {
                            var tzcbm = abm.Where(x => x.scoring_factor_id == tzc.FirstOrDefault().scoring_factor_id).ToList();
                            if (tzcbm.Count() > 0)
                            {
                                tzcbm.FirstOrDefault().weight = tzc.FirstOrDefault().weight ?? 0;
                                tzcbm.FirstOrDefault().benchmark_value_min = tzc.FirstOrDefault().benchmark_value_min ?? 0;
                                tzcbm.FirstOrDefault().benchmark_value_max = tzc.FirstOrDefault().benchmark_value_max ?? 0;
                                tzcbm.FirstOrDefault().last_updated_dt = DateTime.Now;
                                tzcbm.FirstOrDefault().calculated = tzc.FirstOrDefault().calculated ?? false;
                            }
                        }

                    }

                    isdb.SaveChanges();

                    return Ok(_prof);
                }
                else
                {
                    return BadRequest("Update failed.");
                }
            }
        }

        [Authorize]
        [HttpPost]
        [Route("Asset/Delete")]
        public IHttpActionResult DeleteProfile(int profile_id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.Profile.Where(x => x.profile_id == profile_id).ToList();

                if (checkifExist.Count() > 0)
                {

                    var checkIfAssigned = isdb.ProfileAssets.Where(x => x.profile_id == profile_id).ToList();

                    if (checkIfAssigned.Count() > 0)
                    {
                        return BadRequest("Profile has assignment. Remove it first.");
                    }
                    else
                    {

                        var profile = isdb.Profile.Remove(checkifExist.FirstOrDefault());

                        //var profasset = isdb.ProfileAssets.Where(x => x.profile_id == profile_id).ToList();

                        //if (profasset.Count() > 0)
                        //{
                        //    profasset.ForEach(x => isdb.ProfileAssets.Remove(x));


                        //}
                        //else {

                        var abmlist = isdb.AssetBenchmark.Where(x => x.profile_id == profile_id).ToList();

                        abmlist.ForEach(x => isdb.AssetBenchmark.Remove(x));
                        //}

                    }


                }
                isdb.SaveChanges();
                return Ok("Delete successfully.");

            }

        }
        #endregion


        
        #region driver

        [Authorize]
        [Route("Driver/List")]
        [HttpGet]
        public IHttpActionResult ProfileDriverList()
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var assetbenchmarklist = isdb.AssetBenchmark.ToList();

                var scoringfactorlist = isdb.ScoringFactor.ToList();

                var profile = (from i in isdb.Profile
                               where i.user_id == AccountID && i.type == "driver"
                               select new ProfileAll()
                               {
                                   profile_id = i.profile_id,
                                   name = i.name,
                                   description = i.description,
                                   user_id = i.user_id ?? 0,
                                   type = i.type
                               }).OrderBy(x => x.name).ToList();


                //profile.ForEach(b => b.AssetBenchmarkLists.AddRange(
                //    assetbenchmarklist.Where(x => x.profile_id == b.profile_id).Select(c => new AssetBenchmarkList()
                //    {
                //        asset_benchmark_id = c.asset_benchmark_id,
                //        scoring_factor_id = c.scoring_factor_id,
                //        name = c.ScoringFactor.name,
                //        asset_id = c.asset_id,
                //        calculated = c.calculated,
                //        benchmark_value_min = c.benchmark_value_min ?? 0,
                //        benchmark_value_max = c.benchmark_value_max ?? 0,
                //        last_updated_dt = c.last_updated_dt,
                //        weight = c.weight,
                //        profile_id = c.profile_id ?? 0
                //    }).ToList().DistinctBy(x => x.name)
                //    ));

                profile.ForEach(c => c.DriverToleranceLists.AddRange(
                    isdb.DriverTolerance.Where(x => x.profile_id == c.profile_id).Select(d => new DriverToleranceList()
                    {
                        driver_tolerance_id = d.driver_tolerance_id,
                        scoring_factor_id = d.scoring_factor_id,
                        name = d.ScoringFactor.name,
                        driver_id = d.driver_id,
                        calculated = d.calculated,
                        tolerance_value_min = d.tolerance_value_min,
                        tolerance_value_max = d.tolerance_value_max,
                        last_updated_dt = d.last_updated_dt,
                        weight = d.weight,
                        profile_id = d.profile_id ?? 0
                    }).ToList()
                    ));

                return Ok(profile);

            }
        }

        [Authorize]
        [Route("Driver/Info")]
        [HttpGet]
        public IHttpActionResult ProfileDriverInfo(int profile_id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.Profile.Where(x => x.profile_id == profile_id).ToList();

                if (checkifExist.Count() > 0)
                {
                    var profile = checkifExist.Select(i => new ProfileAll()
                    {
                        profile_id = i.profile_id,
                        name = i.name,
                        description = i.description,
                        user_id = i.user_id ?? 0,
                    }).FirstOrDefault();

                    profile.DriverToleranceLists.AddRange(
                        isdb.DriverTolerance.Where(x => x.profile_id == profile.profile_id).Select(c => new DriverToleranceList()
                        {
                            driver_tolerance_id = c.driver_tolerance_id,
                            scoring_factor_id = c.scoring_factor_id,
                            name = c.ScoringFactor.name,
                            driver_id = c.driver_id,
                            calculated = c.calculated,
                            tolerance_value_max = c.tolerance_value_max ?? 0,
                            tolerance_value_min = c.tolerance_value_min ?? 0,
                            last_updated_dt = c.last_updated_dt,
                            weight = c.weight,
                            profile_id = c.profile_id ?? 0
                        }).ToList().DistinctBy(x => x.name)
                    );

                    return Ok(profile);
                }
                else
                {
                    return BadRequest("Profile not found.");
                }
            }
        }

        [Authorize]
        [Route("Driver/Add")]
        [HttpPost]
        public IHttpActionResult AddDriverProfile(ProfileAll _prof)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifProfilenameExist = isdb.Profile.Where(x => x.name == _prof.name && x.type == "driver" && x.user_id == AccountID).ToList();

                if (checkifProfilenameExist.Count() > 0)
                {
                    return BadRequest("Profile name is already exists.");
                }

                Profile profile = new Profile
                {
                    name = _prof.name,
                    description = _prof.description,
                    user_id = AccountID,
                    type = "driver"
                };
                isdb.Profile.Add(profile);

                var drivertolerancelist = _prof.DriverToleranceLists.ToList();

                if (drivertolerancelist.Count() > 0)
                {
                    foreach (var a in drivertolerancelist.ToList())
                    {
                        DriverTolerance ddt = new DriverTolerance
                        {
                            scoring_factor_id = a.scoring_factor_id,
                            //driver_id = a.driver_id,
                            calculated = a.calculated ?? false,
                            tolerance_value_min = a.tolerance_value_min,
                            tolerance_value_max = a.tolerance_value_max,
                            last_updated_dt = DateTime.Now,
                            weight = a.weight ?? 0,
                            profile_id = profile.profile_id
                        };
                        isdb.DriverTolerance.Add(ddt);
                    }
                }

                isdb.SaveChanges();

                return Ok(_prof);
            }
        }

        [Authorize]
        [Route("Driver/Assign")]
        [HttpPost]
        public IHttpActionResult AssignDriverProfile(ProfileDriverAssign _profassign)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.ProfileDriver.Where(x => _profassign.driver_ids.Contains(x.driver_id ?? 0)).ToList();

                if (checkifExist.Count() == 0)
                {
                    foreach (var d in _profassign.driver_ids)
                    {
                        ProfileDriver _prof = new ProfileDriver()
                        {
                            profile_id = _profassign.profile_id,
                            driver_id = d
                        };

                        isdb.ProfileDriver.Add(_prof);
                    }

                        isdb.SaveChanges();

                    return Ok("Assign successfully.");
                }
                else
                {
                    return BadRequest("Already assigned.");
                }
            }
        }

        [Authorize]
        [Route("Driver/DeAssign")]
        [HttpPost]
        public IHttpActionResult DriverDeAssignProfile(ProfileDriverAssign _profassign)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var _profdrivers = isdb.ProfileDriver.Where(x => _profassign.driver_ids.Contains(x.driver_id ?? 0) && x.profile_id == _profassign.profile_id).ToList();

                if (_profdrivers.Count() > 0)
                {
                    _profdrivers.ForEach(x => isdb.ProfileDriver.Remove(x));

                    isdb.SaveChanges();

                    return Ok("Remove Assignment successfully.");
                }
                else
                {
                    return BadRequest("Can't find profile.");
                }

               
            }
        }

        [Authorize]
        [Route("Driver/Assign/List")]
        [HttpGet]
        public IHttpActionResult DriverAssignProfileList(int driverid)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
                {

                    var profile = (from i in isdb.Profile
                                   where i.user_id == AccountID && i.type == "driver"
                                   select new ProfileAll()
                                   {
                                       profile_id = i.profile_id,
                                       name = i.name,
                                       description = i.description,
                                       user_id = i.user_id ?? 0,
                                       type = i.type,
                                   }).OrderBy(x => x.name).ToList();

                    profile.ForEach(x => x.isAssign = isdb.ProfileDriver.Where(c => c.profile_id == x.profile_id && c.driver_id == driverid).Count() > 0 ? true : false);

                    profile.ForEach(c => c.DriverToleranceLists.AddRange(
                     isdb.DriverTolerance.Where(x => x.profile_id == c.profile_id).Select(d => new DriverToleranceList()
                     {
                         driver_tolerance_id = d.driver_tolerance_id,
                         scoring_factor_id = d.scoring_factor_id,
                         name = d.ScoringFactor.name,
                         driver_id = d.driver_id,
                         calculated = d.calculated,
                         tolerance_value_min = d.tolerance_value_min,
                         tolerance_value_max = d.tolerance_value_max,
                         last_updated_dt = d.last_updated_dt,
                         weight = d.weight,
                         profile_id = d.profile_id ?? 0
                     }).ToList()
                     ));

                    return Ok(profile);

                    //var driverList = new List<Driver>();

                    //var result = tfdb.sp_PGPS_get_summary_info_sysuser(AccountID).ToList();

                    //foreach (var res in result)
                    //{
                    //    var driver = tfdb.sp_PGPS_TRP_GetCurrentActiveDriver(res.AssetID).ToList();

                    //    if (driver.Count() > 0)
                    //    {
                    //        driverList.Add(new Driver()
                    //        {
                    //            asset_id = res.AssetID,
                    //            driver_id = driver.FirstOrDefault().DriverId ?? 0,
                    //            name = driver.FirstOrDefault().DriverName,
                    //        });
                    //    }
                    //}

                    //var profdriver = isdb.ProfileDriver.Where(x => x.profile_id == profile_id).ToList();

                    //if (profdriver.Count() > 0)
                    //{
                    //    foreach (var dr in driverList.ToList())
                    //    {
                    //        dr.isAssign = profdriver.ToList().Where(x => x.driver_id == dr.driver_id).ToList().Count() > 0 ? true : false;
                    //    }

                    //}
                    //else
                    //{
                    //    driverList.ForEach(x => x.isAssign = false);
                    //}


                    //return Ok(driverList.ToList());

                }
            }

        }


        [Authorize]
        [HttpPost]
        [Route("Driver/Update")]
        public IHttpActionResult UpdateDriverProfile(ProfileAll _prof)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.Profile.Where(x => x.profile_id == _prof.profile_id).ToList();

                if (checkifExist.Count() > 0)
                {
                    var checkifProfilenameExist = isdb.Profile.Where(x => x.name == _prof.name && x.type == "driver" && x.user_id == AccountID && x.profile_id != _prof.profile_id).ToList();

                    if (checkifProfilenameExist.Count() > 0)
                    {
                        return BadRequest("Profile name is already exists.");
                    }

                    var profile = checkifExist.FirstOrDefault();
                    profile.name = _prof.name;
                    profile.description = _prof.description;

                    var ddt = isdb.DriverTolerance.Where(x => x.profile_id == _prof.profile_id).ToList();

                    if (_prof.DriverToleranceLists.ToList().Count() > 0)
                    {
                        var hb = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Harsh Braking").ToList();
                        var hbdt = ddt.Where(x => x.scoring_factor_id == hb.FirstOrDefault().scoring_factor_id).ToList();
                        if (hbdt.Count() > 0)
                        {
                            hbdt.FirstOrDefault().weight = hb.FirstOrDefault().weight ?? 0;
                            hbdt.FirstOrDefault().tolerance_value_min = hb.FirstOrDefault().tolerance_value_min ?? 0;
                            hbdt.FirstOrDefault().tolerance_value_max = hb.FirstOrDefault().tolerance_value_max ?? 0;
                            hbdt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            hbdt.FirstOrDefault().calculated = hb.FirstOrDefault().calculated ?? false;
                        }

                        var ha = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Harsh Acceleration").ToList();
                        var hadt = ddt.Where(x => x.scoring_factor_id == ha.FirstOrDefault().scoring_factor_id).ToList();
                        if (hadt.Count() > 0)
                        {
                            hadt.FirstOrDefault().weight = ha.FirstOrDefault().weight ?? 0;
                            hadt.FirstOrDefault().tolerance_value_min = ha.FirstOrDefault().tolerance_value_min ?? 0;
                            hadt.FirstOrDefault().tolerance_value_max = ha.FirstOrDefault().tolerance_value_max ?? 0;
                            hadt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            hadt.FirstOrDefault().calculated = ha.FirstOrDefault().calculated ?? false;
                        }

                        var ht = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Harsh Turning").ToList();
                        var htdt = ddt.Where(x => x.scoring_factor_id == ht.FirstOrDefault().scoring_factor_id).ToList();
                        if (htdt.Count() > 0)
                        {
                            htdt.FirstOrDefault().weight = ht.FirstOrDefault().weight ?? 0;
                            htdt.FirstOrDefault().tolerance_value_min = ht.FirstOrDefault().tolerance_value_min ?? 0;
                            htdt.FirstOrDefault().tolerance_value_max = ht.FirstOrDefault().tolerance_value_max ?? 0;
                            htdt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            htdt.FirstOrDefault().calculated = ht.FirstOrDefault().calculated ?? false;
                        }

                        var os = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Over Speed").ToList();
                        var osdt = ddt.Where(x => x.scoring_factor_id == os.FirstOrDefault().scoring_factor_id).ToList();
                        if (osdt.Count() > 0)
                        {
                            osdt.FirstOrDefault().weight = os.FirstOrDefault().weight ?? 0;
                            osdt.FirstOrDefault().tolerance_value_min = os.FirstOrDefault().tolerance_value_min ?? 0;
                            osdt.FirstOrDefault().tolerance_value_max = os.FirstOrDefault().tolerance_value_max ?? 0;
                            osdt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            osdt.FirstOrDefault().calculated = os.FirstOrDefault().calculated ?? false;
                        }

                        var od = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Over Drive").ToList();
                        var oddt = ddt.Where(x => x.scoring_factor_id == od.FirstOrDefault().scoring_factor_id).ToList();
                        if (oddt.Count() > 0)
                        {
                            oddt.FirstOrDefault().weight = od.FirstOrDefault().weight ?? 0;
                            oddt.FirstOrDefault().tolerance_value_min = od.FirstOrDefault().tolerance_value_min ?? 0;
                            oddt.FirstOrDefault().tolerance_value_max = od.FirstOrDefault().tolerance_value_max ?? 0;
                            oddt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            oddt.FirstOrDefault().calculated = od.FirstOrDefault().calculated ?? false;
                        }

                        var op = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Over Park").ToList();
                        var opdt = ddt.Where(x => x.scoring_factor_id == op.FirstOrDefault().scoring_factor_id).ToList();
                        if (opdt.Count() > 0)
                        {
                            opdt.FirstOrDefault().weight = op.FirstOrDefault().weight ?? 0;
                            opdt.FirstOrDefault().tolerance_value_min = op.FirstOrDefault().tolerance_value_min ?? 0;
                            opdt.FirstOrDefault().tolerance_value_max = op.FirstOrDefault().tolerance_value_max ?? 0;
                            oddt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            oddt.FirstOrDefault().calculated = op.FirstOrDefault().calculated ?? false;
                        }

                        var oi = _prof.DriverToleranceLists.ToList().Where(x => x.name == "Over Idle").ToList();
                        var oidt = ddt.Where(x => x.scoring_factor_id == oi.FirstOrDefault().scoring_factor_id).ToList();
                        if (oidt.Count() > 0)
                        {
                            oidt.FirstOrDefault().weight = oi.FirstOrDefault().weight ?? 0;
                            oidt.FirstOrDefault().tolerance_value_min = oi.FirstOrDefault().tolerance_value_min ?? 0;
                            oidt.FirstOrDefault().tolerance_value_max = oi.FirstOrDefault().tolerance_value_max ?? 0;
                            oidt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            oidt.FirstOrDefault().calculated = oi.FirstOrDefault().calculated ?? false;
                        }

                        var nz = _prof.DriverToleranceLists.ToList().Where(x => x.name == "No Zone").ToList();
                        var nzdt = ddt.Where(x => x.scoring_factor_id == nz.FirstOrDefault().scoring_factor_id).ToList();
                        if (nzdt.Count() > 0)
                        {
                            nzdt.FirstOrDefault().weight = nz.FirstOrDefault().weight ?? 0;
                            nzdt.FirstOrDefault().tolerance_value_min = nz.FirstOrDefault().tolerance_value_min ?? 0;
                            nzdt.FirstOrDefault().tolerance_value_max = nz.FirstOrDefault().tolerance_value_max ?? 0;
                            nzdt.FirstOrDefault().last_updated_dt = DateTime.Now;
                            nzdt.FirstOrDefault().calculated = nz.FirstOrDefault().calculated ?? false;
                        }

                    }

                    isdb.SaveChanges();

                    return Ok(_prof);
                }
                else
                {
                    return BadRequest("Update failed.");
                }
            }
        }


        [Authorize]
        [HttpPost]
        [Route("Driver/Delete")]
        public IHttpActionResult DriverDeleteProfile(int profile_id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {
                var checkifExist = isdb.Profile.Where(x => x.profile_id == profile_id).ToList();

                if (checkifExist.Count() > 0)
                {
                    var profdriver = isdb.ProfileDriver.Where(x => x.profile_id == profile_id).ToList();

                    if (profdriver.Count() > 0)
                    {
                        return BadRequest("Profile has assignment. Remove it first.");
                    }
                    else
                    {
                        var profile = isdb.Profile.Remove(checkifExist.FirstOrDefault());

                        //if (profdriver.Count() > 0)
                        //{
                        //    profdriver.ForEach(x => isdb.ProfileDriver.Remove(x));

                        var dtlist = isdb.DriverTolerance.Where(x => x.profile_id == profile_id).ToList();

                        dtlist.ForEach(x => isdb.DriverTolerance.Remove(x));

                        //}
                        //else
                        //{
                        //}
                    }


                }

                isdb.SaveChanges();

                return Ok("Delete successfully.");
            }
        }

        #endregion



    }
}
