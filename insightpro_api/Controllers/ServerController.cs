﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using insightpro_api.DAL;

namespace insightpro_api.Controllers
{
    [RoutePrefix("Server")]
    public class ServerController : ApiController
    {
        public class Server
        {
            public string name { get; set; }
            public string url { get; set; }
            public string code { get; set; }
            public string configurationname { get; set; }
            public string defaultcoordinates { get; set; }
            public string imagesurl { get; set; }
        }

        [Route("List/{_Code}")]
        [HttpGet]
        public IHttpActionResult GetServerList(string _Code)
        {
            using (SERVERDBEntities sdb = new SERVERDBEntities())
            {
                var result = (from s in sdb.WebServers
                              where s.code == _Code
                              select new Server()
                              {
                                  code = s.code,
                                  configurationname = s.config_name,
                                  name = s.name,
                                  url = s.url,
                                  defaultcoordinates = s.default_coordinates,
                                  imagesurl = s.images_url
                              }).ToList();
                if (result.Count() > 0)
                {
                    return Ok(result);
                }
                else {
                    return BadRequest("No Servers available");
                }
            }
        }
    }
}
