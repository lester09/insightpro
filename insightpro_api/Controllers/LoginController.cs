﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using insightpro_api.DAL;
using System.Security.Claims;
using insightpro_api.Helpers;

namespace insightpro_api.Controllers
{
    public class Account
    {
        public int accountid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int? parentid { get; set; }
    }

    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public IHttpActionResult Login(Account _Account)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
            {
                var result = (from i in tfdb.PGPS_SystemUser.ToList()
                              where i.username == _Account.username
                              && i.password == _Account.password
                              select new Account()
                              {
                                  accountid = i.user_id,
                                  password = i.password,
                                  username = i.username,
                                  email = i.email,
                                  parentid = i.parent_user_id,
                              }).ToList();
                if (result.Count() > 0)
                {
                    return Ok("Login successfully");
                }
                else {
                    return BadRequest("Login failed");
                }

            }
        }
    }
}
