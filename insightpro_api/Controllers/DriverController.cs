﻿using insightpro_api.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using insightpro_api.Helpers;

namespace insightpro_api.Controllers
{
    public class Driver
    {
        public int driver_id { get; set; }
        public string name { get; set; }
        public string asset_id { get; set; }
        public bool isAssign { get; set; }
        public int profile_id { get; set; }
        public string profile_name { get; set; }
    }

    public class DriverScoreSummary
    {
        public string driver_id { get; set; }
        public string driver_name { get; set; }
        public double driver_cs { get; set; }
        public double driver_ps { get; set; }
        public double driver_as_all { get; set; }
        public string year_month { get; set; }
        public List<DriverFrequencySummary> data { get; set; }

        public double harsh_braking_c { get; set; }
        public double harsh_acceleration_c { get; set; }
        public double harsh_turning_c { get; set; }
        public double over_speed_c { get; set; }
        public double over_drive_c { get; set; }
        public double over_park_c { get; set; }
        public double over_idle_c { get; set; }
        public double no_zone_c { get; set; }

        public double harsh_braking_p { get; set; }
        public double harsh_acceleration_p { get; set; }
        public double harsh_turning_p { get; set; }
        public double over_speed_p { get; set; }
        public double over_drive_p { get; set; }
        public double over_park_p { get; set; }
        public double over_idle_p { get; set; }
        public double no_zone_p { get; set; }

        public DriverScoreSummary()
        {
            this.data = new List<DriverFrequencySummary>();
        }

    }

    public class DriverFrequencySummary
    {
        public string factor_type { get; set; }
        public double frequency_total { get; set; }
        public double frequency_average { get; set; }
        public double frequency_max { get; set; }
        public double frequency_total_avg_all { get; set; }
        public double previous_total { get; set; }
        public double fa_previous { get; set; }
        public double fa_current { get; set; }
        public List<DriverDailyFrequencySummary> data { get; set; }

        public DriverFrequencySummary()
        {
            this.data = new List<DriverDailyFrequencySummary>();
        }
    }

    public class DriverDailyFrequencySummary
    {
        public double cf { get; set; }
        public double pf { get; set; }
        public string day { get; set; }
        public double af { get; set; }
        public double paf { get; set; }
        public double bm { get; set; }
        public double sf { get; set; }


        public double cs { get; set; }
        public double ps { get; set; }
    }

    public class DriverDailyScore
    {
        public double[] hb { get; set; }
        public double[] ha { get; set; }
        public double[] ht { get; set; }
        public double[] os { get; set; }
        public double[] od { get; set; }
        public double[] op { get; set; }
        public double[] oi { get; set; }
        public double[] nz { get; set; }
        public double[] score { get; set; }
        public string[] day { get; set; }

        public double[] m_hb { get; set; }
        public double[] m_ha { get; set; }
        public double[] m_ht { get; set; }
        public double[] m_os { get; set; }
        public double[] m_od { get; set; }
        public double[] m_op { get; set; }
        public double[] m_oi { get; set; }
        public double[] m_nz { get; set; }

        public double avgscore { get; set; }
        public double avghb { get; set; }
        public double avgha { get; set; }
        public double avght { get; set; }
        public double avgos { get; set; }
        public double avgod { get; set; }
        public double avgop { get; set; }
        public double avgoi { get; set; }
        public double avgnz { get; set; }


        public DriverDailyScore(int year, int month)
        {
            var daysinmonth = DriverController.AllDatesInMonth(year, month).Count();

            hb = new double[daysinmonth];
            ha = new double[daysinmonth];
            ht = new double[daysinmonth];
            os = new double[daysinmonth];
            od = new double[daysinmonth];
            op = new double[daysinmonth];
            oi = new double[daysinmonth];
            nz = new double[daysinmonth];
            score = new double[daysinmonth];
            day = new string[daysinmonth];

            m_hb = new double[daysinmonth];
            m_ha = new double[daysinmonth];
            m_ht = new double[daysinmonth];
            m_os = new double[daysinmonth];
            m_od = new double[daysinmonth];
            m_op = new double[daysinmonth];
            m_oi = new double[daysinmonth];
            m_nz = new double[daysinmonth];
        }
    }

    public class DriverMonthlyScore
    {


        public double avgscore { get; set; }
        public double avghb { get; set; }
        public double avgha { get; set; }
        public double avght { get; set; }
        public double avgos { get; set; }
        public double avgod { get; set; }
        public double avgop { get; set; }
        public double avgoi { get; set; }
        public double avgnz { get; set; }


    }

    public class DriverParam
    {
        public int year { get; set; }
        public int month { get; set; }
    }

    [RoutePrefix("Driver")]
    public class DriverController : ApiController
    {
        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        [Authorize]
        [Route("List")]
        [HttpGet]
        public IHttpActionResult DriverList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
            {
                using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
                {



                    var driverList = new List<Driver>();

                    var result = tfdb.sp_PGPS_get_summary_info_sysuser(AccountID).ToList();

                    foreach (var res in result)
                    {
                        var driver = tfdb.sp_PGPS_TRP_GetCurrentActiveDriver(res.AssetID).ToList();

                        if (driver.Count() > 0)
                        {
                            driverList.Add(new Driver()
                            {
                                asset_id = res.AssetID,
                                driver_id = driver.FirstOrDefault().DriverId ?? 0,
                                name = driver.FirstOrDefault().DriverName,
                                //profile_id = profileid
                                //profile_id = isdb.ProfileDriver.Where(x => x.driver_id == driver.FirstOrDefault().DriverId).Count() > 0 ? isdb.ProfileDriver.Where(x => x.driver_id == driver.FirstOrDefault().DriverId).FirstOrDefault().profile_id ?? 0 : 0,
                            });

                        }
                    }

                    //driverList.ForEach(x => x.profile_id = isdb.ProfileDriver.Where(c => c.driver_id == c.driver_id).Count() > 0 ? isdb.ProfileDriver.Where(c => c.driver_id == c.driver_id).FirstOrDefault().profile_id ?? 0 : 0);

                    foreach (var driver in driverList.ToList())
                    {
                        driver.profile_id = isdb.ProfileDriver.Where(c => c.driver_id == driver.driver_id).Count() > 0 ? isdb.ProfileDriver.Where(c => c.driver_id == driver.driver_id).FirstOrDefault().profile_id ?? 0 : 0;
                        driver.profile_name = isdb.Profile.Where(c => c.profile_id == driver.profile_id).Count() > 0 ? isdb.Profile.Where(c => c.profile_id == driver.profile_id).FirstOrDefault().name : null;
                        driver.isAssign = driver.profile_id == 0 ? false : true;
                    }



                    return Ok(driverList);
                }
            }
        }


        [Authorize]
        [Route("Daily/Score")]
        [HttpGet]
        public IHttpActionResult DailyAssetScore(string id, int year, int month)
        {
            DateTime startdtc = AllDatesInMonth(year, month).FirstOrDefault();
            DateTime enddtc = AllDatesInMonth(year, month).LastOrDefault();

            DateTime startdtp = startdtc.AddMonths(-1);
            DateTime enddtp = enddtc.AddMonths(-1);


            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var resultc = isdb.sp_calculate_driver_score(id, startdtc, enddtc).ToList();

                var h = new DriverDailyScore(year, month);



                foreach (var day in AllDatesInMonth(year, month))
                {
                    var cc = resultc.Where(x => x.Day.Value.Day == day.Day).ToList();

                    h.day[day.Day - 1] = day.Day.ToString("00");
                    h.score[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().DriverScore : 0;
                    h.hb[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Harsh_Braking : 0;
                    h.ha[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Harsh_Acceleration: 0;
                    h.ht[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Harsh_Turning : 0;
                    h.os[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().OverSpeed : 0;
                    h.od[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().OverDrive : 0;
                    h.op[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().OverPark  : 0;
                    h.oi[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().OverIdle : 0;
                    h.nz[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().NoZone : 0;

                    h.m_hb[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_hb : 0;
                    h.m_ha[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_ha : 0;
                    h.m_ht[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_ht : 0;
                    h.m_os[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_os : 0;
                    h.m_od[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_od : 0;
                    h.m_op[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_op : 0;
                    h.m_oi[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_oi : 0;
                    h.m_nz[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_nz : 0;

                }

                if (resultc.Count() == 0)
                {
                    h.avgscore = 0;
                    h.avghb = 0;
                    h.avgha = 0;
                    h.avght = 0;
                    h.avgos = 0;
                    h.avgod = 0;
                    h.avgop = 0;
                    h.avgoi = 0;
                    h.avgnz = 0;
                }
                else
                {
                    //h.avgscore = h.score.ToList().Where(x => x > 0).Count() > 0 ? h.score.ToList().Where(x=> x > 0).Average() : 0;
                    h.avgscore = h.score.ToList().Where(x => x > 0).Count() > 0 ? h.score.ToList().Where(x=> x > 0).LastOrDefault() : 0;
                    h.avghb = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.Harsh_Braking).Average() : 0;
                    h.avgha = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.Harsh_Acceleration).Average() : 0;
                    h.avght = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.Harsh_Turning).Average() : 0;
                    h.avgos = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.OverSpeed).Average() : 0;
                    h.avgod = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.OverDrive).Average() : 0;
                    h.avgop = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.OverPark).Average() : 0;
                    h.avgoi = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.OverIdle).Average() : 0;
                    h.avgnz = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.DriverScore > 0).Select(x=> x.NoZone).Average() : 0;
            

                }

                return Ok(h);



            }
        }

        [Authorize]
        [HttpGet]
        [Route("Monthly/Score")]
        public IHttpActionResult MonthlyDriverScore(int month, int year)
        {



            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            DateTime startdtc = AllDatesInMonth(year, month).FirstOrDefault();
            DateTime enddtc = AllDatesInMonth(year, month).LastOrDefault();

            DateTime startdtp = startdtc.AddMonths(-1);
            DateTime enddtp = enddtc.AddMonths(-1);

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var resultc = isdb.sp_calculate_driver_score_monthly(AccountID.ToString(), startdtc, enddtc).ToList();

                var h = new DriverMonthlyScore();

                if (resultc.Count() == 0)
                {
                    var r = new DriverMonthlyScore();
                    r.avgscore = 0;
                    r.avghb = 0;
                    r.avgha = 0;
                    r.avght = 0;
                    r.avgos = 0;
                    r.avgod = 0;
                    r.avgop = 0;
                    r.avgoi = 0;
                    r.avgnz = 0;

                    return Ok(r);

                }
                else
                {
                    h.avgscore = resultc.Average(x => x.DriverScore ?? 0);
                    h.avghb = resultc.Average(x => x.Harsh_Braking ?? 0);
                    h.avgha = resultc.Average(x => x.Harsh_Acceleration ?? 0);
                    h.avght = resultc.Average(x => x.Harsh_Turning ?? 0);
                    h.avgos = resultc.Average(x => x.OverSpeed ?? 0);
                    h.avgod = resultc.Average(x => x.OverDrive ?? 0);
                    h.avgop = resultc.Average(x => x.OverPark ?? 0);
                    h.avgoi = resultc.Average(x => x.OverIdle ?? 0);
                    h.avgnz = resultc.Average(x => x.NoZone ?? 0);

                    return Ok(h);

                }


            }
        }
    }
}
