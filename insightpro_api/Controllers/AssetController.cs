﻿using insightpro_api.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using insightpro_api.Helpers;

namespace insightpro_api.Controllers
{
    public class AssetScoreSummary
    {
        public string asset_id { get; set; }
        public string asset_name { get; set; }
        public double asset_cs { get; set; }
        public double asset_ps { get; set; }
        public double asset_as_all { get; set; }
        public string year_month { get; set; }
        public List<AssetFrequencySummary> data { get; set; }

        public double travel_duration_c { get; set; }
        public double idle_duration_c { get; set; }
        public double parking_duration_c { get; set; }
        public double distance_travelled_c { get; set; }
        public double trip_count_c { get; set; }
        public double trip_zone_count_c { get; set; }

        public double travel_duration_p { get; set; }
        public double idle_duration_p { get; set; }
        public double parking_duration_p { get; set; }
        public double distance_travelled_p { get; set; }
        public double trip_count_p { get; set; }
        public double trip_zone_count_p { get; set; }

        public AssetScoreSummary()
        {
            this.data = new List<AssetFrequencySummary>();
        }

    }

    public class AssetFrequencySummary
    {
        public string factor_type { get; set; }
        public double frequency_total { get; set; }
        public double frequency_average { get; set; }
        public double frequency_max { get; set; }
        public double frequency_total_avg_all { get; set; }
        public double previous_total { get; set; }
        public double fa_previous { get; set; }
        public double fa_current { get; set; }
        public List<AssetDailyFrequencySummary> data { get; set; }

        public AssetFrequencySummary()
        {
            this.data = new List<AssetDailyFrequencySummary>();
        }
    }

    public class AssetDailyFrequencySummary
    {
        public double cf { get; set; }
        public double pf { get; set; }
        public string day { get; set; }
        public double af { get; set; }
        public double paf { get; set; }
        public double bm { get; set; }
        public double sf { get; set; }


        public double cs { get; set; }
        public double ps { get; set; }
    }

    public class AssetDailyScore
    {
        public double[] td { get; set; }
        public double[] tpd { get; set; }
        public double[] dt { get; set; }
        public double[] tc { get; set; }
        public double[] tcz { get; set; }
        public double[] m_td { get; set; }
        public double[] m_tpd { get; set; }
        public double[] m_dt { get; set; }
        public double[] m_tc { get; set; }
        public double[] m_tcz { get; set; }
        public double[] score { get; set; }
        public string[] day { get; set; }

        public double avgscore { get; set; }
        public double avgtd { get; set; }
        public double avgtpd { get; set; }
        public double avgdt { get; set; }
        public double avgtc { get; set; }
        public double avgtcz { get; set; }


        public AssetDailyScore(int year, int month)
        {
            var daysinmonth = AssetController.AllDatesInMonth(year, month).Count();

            td = new double[daysinmonth];
            tpd = new double[daysinmonth];
            dt = new double[daysinmonth];
            tc = new double[daysinmonth];
            tcz = new double[daysinmonth];

            m_td = new double[daysinmonth];
            m_tpd = new double[daysinmonth];
            m_dt = new double[daysinmonth];
            m_tc = new double[daysinmonth];
            m_tcz = new double[daysinmonth];

            score = new double[daysinmonth];
            day = new string[daysinmonth];
        }
    }

    public class AssetMonthlyScore
    {

        public double avgscore { get; set; }
        public double avgtd { get; set; }
        public double avgtpd { get; set; }
        public double avgdt { get; set; }
        public double avgtc { get; set; }
        public double avgtcz { get; set; }


      
    }

    public class Asset
    {
        public string asset_id { get; set; }
        public string asset_name { get; set; }
        public int? customer_id { get; set; }
        public string customer_name { get; set; }
        public AssetScoreSummary asset_score_summary { get; set; }
        public bool? isAssign { get; set; }
        public int? profile_id { get; set; }
        public string profile_name { get; set; }
    }


    [RoutePrefix("Asset")]
    public class AssetController : ApiController
    {

        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        [Route("Daily/Score")]
        [HttpGet]
        public IHttpActionResult DailyAssetScore(string id, int year, int month)
        {
            DateTime startdtc = AllDatesInMonth(year, month).FirstOrDefault();
            DateTime enddtc = AllDatesInMonth(year, month).LastOrDefault();

            DateTime startdtp = startdtc.AddMonths(-1);
            DateTime enddtp = enddtc.AddMonths(-1);


            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var resultc = isdb.sp_calculate_asset_score(id, startdtc, enddtc).ToList();
                var h = new AssetDailyScore(year,month);
             
                if (resultc.Count() == 0)
                {

                    foreach (var day in AllDatesInMonth(year, month))
                    {

                        h.day[day.Day - 1] = day.Day.ToString("00");
                        h.score[day.Day - 1] = 0;
                        h.td[day.Day - 1] = 0;
                        h.tpd[day.Day - 1] = 0;
                        h.dt[day.Day - 1] = 0;
                        h.tc[day.Day - 1] = 0;
                        h.tcz[day.Day - 1] = 0;

                        h.m_td[day.Day - 1] = 0;
                        h.m_tpd[day.Day - 1] = 0;
                        h.m_dt[day.Day - 1] = 0;
                        h.m_tc[day.Day - 1] = 0;
                        h.m_tcz[day.Day - 1] = 0;
                    }

                    h.avgscore = 0;
                    h.avgdt = 0;
                    h.avgtc = 0;
                    h.avgtcz = 0;
                    h.avgtd = 0;
                    h.avgtpd = 0;

                }
                else
                {
                    foreach (var day in AllDatesInMonth(year, month))
                    {
                        var cc = resultc.Where(x => x.Day.Day.ToString().strToInt() == day.Day).ToList();

                        h.day[day.Day - 1] = day.Day.ToString("00");
                        h.score[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().AssetScore : 0;
                        h.td[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Travel_Duration : 0;
                        h.tpd[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Trip_Duration : 0;
                        h.dt[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Distance_Travelled : 0;
                        h.tc[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Trip_Count : 0;
                        h.tcz[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().Trip_Zone_Count : 0;

                        h.m_td[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_td ?? 0 : 0;
                        h.m_tpd[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_tpd ?? 0 : 0;
                        h.m_dt[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_dt ?? 0 : 0;
                        h.m_tc[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_tc ?? 0 : 0;
                        h.m_tcz[day.Day - 1] = cc.Count() > 0 ? cc.FirstOrDefault().raw_tzc ?? 0 : 0;
                    }


                    h.avgscore = h.score.ToList().Where(x => x > 0).Count() > 0 ? h.score.ToList().Where(x => x > 0).Average() : 0;
                    h.avgdt = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.AssetScore > 0).Select(x => x.Distance_Travelled).Average() : 0;
                    h.avgtc = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.AssetScore > 0).Select(x => x.Trip_Count).Average() : 0;
                    h.avgtcz = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.AssetScore > 0).Select(x => x.Trip_Zone_Count).Average() : 0;
                    h.avgtd = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.AssetScore > 0).Select(x => x.Travel_Duration).Average() : 0;
                    h.avgtpd = h.score.ToList().Where(x => x > 0).Count() > 0 ? resultc.ToList().Where(x => x.AssetScore > 0).Select(x => x.Trip_Duration).Average() : 0;

                    //h.avgdt = resultc.ToList().Where(x => x.Distance_Travelled > 0).Select(x => x.Distance_Travelled).Average() ?? 0;
                    //h.avgtc = resultc.ToList().Where(x => x.Trip_Count> 0).Select(x => x.Trip_Count).Average() ?? 0;
                    //h.avgtcz = resultc.ToList().Where(x => x.Trip_Zone_Count > 0).Select(x => x.Trip_Zone_Count).Average() ?? 0;
                    //h.avgtd = resultc.ToList().Where(x => x.Travel_Duration > 0).Select(x => x.Travel_Duration).Average() ?? 0;
                    //h.avgtpd = resultc.ToList().Where(x => x.Trip_Duration > 0).Select(x => x.Trip_Duration).Average() ?? 0;


                }

                return Ok(h);

            }
        }


        [Authorize]
        [HttpGet]
        [Route("Monthly/Score")]
        public IHttpActionResult MonthlyAssetScore(int year, int month)
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            DateTime startdtc = AllDatesInMonth(year, month).FirstOrDefault();
            DateTime enddtc = AllDatesInMonth(year, month).LastOrDefault();

            DateTime startdtp = startdtc.AddMonths(-1);
            DateTime enddtp = enddtc.AddMonths(-1);

            using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
            {

                var resultc = isdb.sp_calculate_asset_score_monthly(AccountID.ToString(), startdtc, enddtc).ToList();

                var h = new AssetMonthlyScore();

                if (resultc.Count() == 0)
                {
                    var r = new AssetMonthlyScore();
                    r.avgscore = 0;
                    r.avgdt = 0;
                    r.avgtc = 0;
                    r.avgtcz = 0;
                    r.avgtd = 0;
                    return Ok(r);

                }
                else {
                    h.avgscore = resultc.FirstOrDefault().AssetScore ?? 0;//resultc.Average(x => x.AssetScore) ?? 0;
                    h.avgdt = resultc.FirstOrDefault().Distance_Travelled ?? 0; //resultc.Average(x => x.Distance_Travelled);
                    h.avgtc = resultc.FirstOrDefault().Trip_Count ?? 0; //resultc.Average(x => x.Trip_Count);
                    h.avgtcz = resultc.FirstOrDefault().Trip_Zone_Count ?? 0; //resultc.Average(x => x.Trip_Zone_Count);
                    h.avgtpd = resultc.FirstOrDefault().Trip_Duration ?? 0; //resultc.Average(x => x.Trip_Duration);
                    h.avgtd = resultc.FirstOrDefault().Trip_Duration ?? 0; //resultc.Average(x => x.Travel_Duration);

                    return Ok(h);

                }


            }
        }

        [Authorize]
        [Route("List")]
        [HttpGet]
        public IHttpActionResult AssetList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
            {

                using (INSIGHTPRODB_PHEntities isdb = new INSIGHTPRODB_PHEntities())
                {

                    var result = (from i in tfdb.sp_PGPS_get_summary_info_sysuser(AccountID).ToList()
                                  select new Asset()
                                  {
                                      asset_id = i.AssetID,
                                      asset_name = i.Name,
                                      customer_id = i.CustomerID,
                                      customer_name = i.CustomerName,
                                      profile_id = isdb.ProfileAssets.Where(c => c.asset_id == i.AssetID).Count() > 0 ? isdb.ProfileAssets.Where(c => c.asset_id == i.AssetID).FirstOrDefault().profile_id : 0,
                                  }).ToList().OrderBy(x => x.asset_name);

                    foreach (var res in result.ToList())
                    {
                        res.profile_name = isdb.Profile.Where(x=> x.profile_id == res.profile_id).Count() > 0 ? isdb.Profile.Where(c => c.profile_id == res.profile_id).ToList().FirstOrDefault().name : null;
                        res.isAssign = res.profile_id == 0 ? false : true;
                    }

                return Ok(result);
                }

            }
        }

        [Authorize]
        [HttpGet]
        [Route("Score/List")]
        public IHttpActionResult AssetScoreList(int year, int month)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> Claims = identity.Claims;
            int AccountID = Claims.Where(x => x.Type == "AccountID").FirstOrDefault().Value.ToString().strToInt();
            string ConfigurationName = Claims.Where(x => x.Type == "ConfigurationName").FirstOrDefault().Value.ToString();
            string WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");

            using (TFDBEntities tfdb = new TFDBEntities(WebConfigurationName))
            {

                var AssetScoreSummaryList = new List<AssetScoreSummary>();

                var result = (from i in tfdb.sp_PGPS_get_summary_info_sysuser(AccountID).ToList()
                              select new Asset()
                              {
                                  asset_id = i.AssetID,
                                  asset_name = i.Name,
                                  customer_id = i.CustomerID,
                                  customer_name = i.CustomerName,
                                  //   asset_score_summary = GetAssetScore(i.AssetID, month, year, WebConfigurationName)
                              }).ToList();

                foreach (var res in result.ToList())
                {
                    AssetScoreSummaryList.Add(new AssetScoreSummary
                    {
                        asset_id = res.asset_id,
                        asset_name = res.asset_name,
                        asset_cs = res.asset_score_summary.asset_cs,
                        asset_ps = res.asset_score_summary.asset_ps,
                        asset_as_all = res.asset_score_summary.asset_as_all,
                        year_month = res.asset_score_summary.year_month,
                        data = res.asset_score_summary.data
                    });
                }

                return Ok(AssetScoreSummaryList);

            }
        }

    }
}
