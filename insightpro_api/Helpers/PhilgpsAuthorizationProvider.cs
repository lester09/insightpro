﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using insightpro_api.DAL;

namespace insightpro_api.Helpers
{
    public class Account
    {
        public int AccountID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Groupcodes { get; set; }
        public string Email { get; set; }
        public int ParentID { get; set; }
    }

    public class PhilgpsAuthorizationProvider : OAuthAuthorizationServerProvider
    {

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var data = await context.Request.ReadFormAsync();
            string ConfigurationName = data.Where(x => x.Key == "db").FirstOrDefault().Value.FirstOrDefault().ToString();

            var WebConfigurationName = ConfigurationName.GetWebConfigurationName("TFDB");
            var DownloadsURL = ConfigurationName.GetDownloadsHistoryURL("TFDB");

            PhilgpsUserRepository _repo = new PhilgpsUserRepository();

            Account result = FindUser(context.UserName,context.Password, WebConfigurationName);

            if (result == null)
            {
                context.SetError("invalid_grant", "The username or password is incorrect.");
                return;
            }
            
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            identity.AddClaim(new Claim("ConfigurationName", ConfigurationName));
            identity.AddClaim(new Claim("AccountID", result.AccountID.ToString()));

            context.Validated(identity);

        }


        public Account FindUser(string _Username, string _Password, string _WebConfigurationName)
        {
            using (TFDBEntities tfdb = new TFDBEntities(_WebConfigurationName))
            {


                var result = (from s in tfdb.PGPS_SystemUser.ToList()
                              where s.username == _Username
                              && s.password == _Password
                              select new Account()
                              {
                                  AccountID = s.user_id,
                                  Groupcodes = "0",
                                  Password = s.password,
                                  Username = s.username,
                                  Email = s.email,
                                  ParentID = s.parent_user_id ?? 0
                              }).ToList();

                if (result.Count() > 0)
                {
                    return result.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
