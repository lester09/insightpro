//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace insightpro_api.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SummaryViolation
    {
        public int summary_violations_id { get; set; }
        public string asset_id { get; set; }
        public Nullable<int> overidle { get; set; }
        public Nullable<int> overspeed { get; set; }
        public Nullable<int> overpark { get; set; }
        public Nullable<int> overdrive { get; set; }
        public Nullable<int> harshbraking { get; set; }
        public Nullable<int> harshacceleration { get; set; }
        public Nullable<System.DateTime> date { get; set; }
    }
}
